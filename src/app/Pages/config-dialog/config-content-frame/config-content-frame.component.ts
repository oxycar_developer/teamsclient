import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {ICONS} from '../../../../shared-web-ressources/shared-angular-web-components/Display/oxy-icons/icon/icon.component';
import {Subject} from 'rxjs';
import {WorkerService} from '../../../../oxy-core/CoreManager/core/CoreServices/worker.service';
import {location, NavigatorService} from '../../../services/navigator.service';
import {AppEvent} from '../../../../oxy-core/events.enum';
import {takeUntil} from 'rxjs/operators';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {C} from '../../../../shared-web-ressources/utils/c';

@Component({
  selector: 'app-config-content-frame',
  templateUrl: './config-content-frame.component.html',
  styleUrls: ['./config-content-frame.component.css']
})
export class ConfigContentFrameComponent implements OnInit, OnDestroy {


  waitingForHome: boolean = false;
  waitingForEntreprise: boolean = false;
  public check_driver: boolean = true;


  erreur: string = '';
  ICONS = ICONS;
  private unsubscribe$ = new Subject();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      title: string
    },
    private mdDialogRef: MatDialogRef<ConfigContentFrameComponent>,
    public w: WorkerService,
    private navigator: NavigatorService,
  ) {
  }


  ngOnInit() {
    this.w.listenForEvent(AppEvent.UpdateUserDone) /// confirms that B2B entreprise was saved
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: value => {
          C.log('ConfigContentFrameComponent ::::: UpdateUserDone this.waitingForEntreprise ::: ', this.waitingForEntreprise);
          this.waitingForEntreprise = false;
        },
      });
    this.w.listenForEvent(AppEvent.SaveB2CEnterpriseLocationDone)/// confirms that B2C entreprise was saved
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: value => {
          C.log('ConfigContentFrameComponent ::::: SaveB2CEnterpriseLocationDone this.waitingForEntreprise ::: ', this.waitingForEntreprise);
          this.waitingForEntreprise = false;
        },
      });
    this.w.listenForEvent(AppEvent.SaveFavoritePointDone) /// confirms that Home was saved
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: value => {
          C.log('ConfigContentFrameComponent :::::  this.waitingForHome ::: ', this.waitingForHome);
          this.waitingForHome = false;
        },
      });
    this.w.listenForEvent(AppEvent.UserSyncDone) /// confirms that Home was saved
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: value => {
          this.checkStatus();
        },
      });
    this.w.listenForEvent(AppEvent.AppIsInit)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: value => {
          C.log('ConfigContentFrameComponent :::::  this.navigator.goTo(location.Home); ::: ');
          this.navigator.goTo(location.Home);
        },
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
  }

  save() {

    if (this.checkHome() && this.checkWork()) {
      this.waitingForHome = true;
      this.waitingForEntreprise = true;
      this.w.emit(AppEvent.RequestSaveFavoritePoint);
      if (this.w.data().auth.apiLoginData.customer.account_configured.account_company_b2b) {
        this.w.emit(AppEvent.RequestSaveB2BEnterpriseLocation);
      } else {
        C.log('ConfigContentFrameComponent :::::  RequestSaveB2CEnterpriseLocation ::: ');
        this.w.emit(AppEvent.RequestSaveB2CEnterpriseLocation);
      }
    }
  }

  stillWorking(): boolean {
    return (this.waitingForEntreprise || this.waitingForHome);
  }

  checkStatus() {
    C.log('ConfigContentFrameComponent :::::  ', this.w.data().user.all.account_configured);
    if (this.stillWorking()) {
      return;
    }

    if (this.w.data().user.all.account_configured.account_fully_configured) {
      this.w.emit(AppEvent.RequestLoginWithApiKey);
    }
  }

  private checkWork() {
    // B2B
    C.log('ConfigContentFrameComponent :::::  this.checkWork ::: ', this.w.data().auth.apiLoginData.customer.account_configured.account_company_b2b);

    if (this.w.data().auth.apiLoginData.customer.account_configured.account_company_b2b) {
      if (this.w.data().interfaces.enterprise.SelectedBuilding
        && this.w.data().interfaces.enterprise.SelectedSite
        && this.w.data().interfaces.enterprise.SelectedBuilding) {
        return true;
      } else {
        this.erreur = 'N\'oubliez pas de saisir un lieu de travail valide';
        return false;
      }
    } else {
      C.log('ConfigContentFrameComponent :::::  B2C ::: ', this.w.data().interfaces.enterprise.SelectedB2CLocation.gps.gps_lon && this.w.data().interfaces.enterprise.SelectedB2CLocation.gps.gps_lat);

      // B2C
      if (this.w.data().interfaces.enterprise.SelectedB2CLocation.gps.gps_lat
        && this.w.data().interfaces.enterprise.SelectedB2CLocation.gps.gps_lon) {
        return true;
      } else {
        this.erreur = 'N\'oubliez pas de saisir un lieu de travail valide';
        return false;
      }
    }
  }

  private checkHome() {
    C.log('ConfigContentFrameComponent :::::  this.checkHome ::: ', this.w.data().settings.userHome, this.w.data().settings.userHome.gps_lat, this.w.data().settings.userHome.gps_lon);

    if (this.w.data().settings.userHome && this.w.data().settings.userHome.gps_lat && this.w.data().settings.userHome.gps_lon) {
      return true;
    }
    this.erreur = 'N\'oubliez pas de saisir une adresse de domicile valide';
    return false;
  }

}
