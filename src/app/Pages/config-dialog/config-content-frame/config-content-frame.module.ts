import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConfigContentFrameComponent} from './config-content-frame.component';
import {ButtonModule} from '../../../../shared-web-ressources/shared-angular-web-components/Inputs/button/button.module';
import {MatDialogModule} from '@angular/material/dialog';
import {GmapAutoCompleteModule} from '../../../../shared-web-ressources/shared-angular-web-components/Inputs/gmap-auto-complete/gmap-auto-complete.module';
import {CitySiteBuildingSetterModule} from '../../../../shared-web-ressources/shared-angular-web-components/Inputs/city-site-building-setter/city-site-building-setter.module';


@NgModule({
  declarations: [ConfigContentFrameComponent],
  exports: [],
  imports: [
    CommonModule,
    MatDialogModule,
    ButtonModule,
    GmapAutoCompleteModule,
    CitySiteBuildingSetterModule
  ],
  entryComponents: [
    ConfigContentFrameComponent
  ]
})
export class ConfigContentFrameModule {
}
