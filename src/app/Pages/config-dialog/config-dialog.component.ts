import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {WorkerService} from '../../../oxy-core/CoreManager/core/CoreServices/worker.service';
import {Subject} from 'rxjs';
import {ConfigContentFrameComponent} from './config-content-frame/config-content-frame.component';

@Component({
  selector: 'app-config-dialog',
  templateUrl: './config-dialog.component.html',
  styleUrls: ['./config-dialog.component.css']
})
export class ConfigDialogComponent implements OnInit, OnDestroy {

  $subscriber = new Subject();


  constructor(
    private dialog: MatDialog,
    private w: WorkerService
  ) {
  }

  ngOnDestroy() {
    this.$subscriber.next();
    this.dialog.closeAll();

  }

  ngOnInit(): void {


    const dialogRef = this.dialog.open(ConfigContentFrameComponent, {
      width: 'auto',
      minWidth: '50vw'
    });


  }

}
