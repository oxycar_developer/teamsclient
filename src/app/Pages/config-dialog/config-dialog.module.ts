import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConfigDialogComponent} from './config-dialog.component';
import {ConfigContentFrameModule} from './config-content-frame/config-content-frame.module';
import {ButtonModule} from '../../../shared-web-ressources/shared-angular-web-components/Inputs/button/button.module';
import {RouterModule, Routes} from '@angular/router';


export const routes: Routes = [
  {
    path: '',
    component: ConfigDialogComponent,
  }
];

@NgModule({
  declarations: [
    ConfigDialogComponent,
  ],
  imports: [
    CommonModule,
    ConfigContentFrameModule,
    ButtonModule,
    RouterModule.forChild(routes)
  ],
  exports: [],
})
export class ConfigDialogModule {
}
