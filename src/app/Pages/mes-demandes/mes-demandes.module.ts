import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MesDemandesComponent} from './mes-demandes.component';
import {RouterModule, Routes} from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    component: MesDemandesComponent,
    children: [
      {
        path: 'config',
        loadChildren: () => import('../config-dialog/config-dialog.module').then(m => m.ConfigDialogModule),
      },
    ]
  }
];

@NgModule({
  declarations: [MesDemandesComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class MesDemandesModule {
}
