import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MesTrajetsComponent} from './mes-trajets.component';
import {RouterModule, Routes} from '@angular/router';
import {PopupDialogsModule} from '../../../shared-web-ressources/shared-angular-web-components/widgets/popup-dialogs/popup-dialogs.module';
import {ButtonModule} from '../../../shared-web-ressources/shared-angular-web-components/Inputs/button/button.module';
import {NouveauTrajetDialogComponent} from './nouveau-trajet-dialog/nouveau-trajet-dialog.component';
import { NouveauTrajetContentFrameComponent } from './nouveau-trajet-dialog/nouveau-trajet-content-frame/nouveau-trajet-content-frame.component';

export const routes: Routes = [
  {
    path: '',
    component: MesTrajetsComponent,
    children: [
      {
        path: 'nouveau',
        component: NouveauTrajetDialogComponent
      },
      {
        path: 'config',
        loadChildren: () => import('../config-dialog/config-dialog.module').then(m => m.ConfigDialogModule),
      },
    ]
  }
];

@NgModule({
  declarations: [MesTrajetsComponent, NouveauTrajetDialogComponent, NouveauTrajetContentFrameComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    PopupDialogsModule,
    ButtonModule,
  ]
})
export class MesTrajetsModule {
}
