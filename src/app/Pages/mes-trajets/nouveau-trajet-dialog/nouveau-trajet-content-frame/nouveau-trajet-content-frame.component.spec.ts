import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NouveauTrajetContentFrameComponent } from './nouveau-trajet-content-frame.component';

describe('NouveauTrajetContentFrameComponent', () => {
  let component: NouveauTrajetContentFrameComponent;
  let fixture: ComponentFixture<NouveauTrajetContentFrameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NouveauTrajetContentFrameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NouveauTrajetContentFrameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
