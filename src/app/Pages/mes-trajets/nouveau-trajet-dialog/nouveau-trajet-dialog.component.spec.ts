import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NouveauTrajetDialogComponent } from './nouveau-trajet-dialog.component';

describe('NouveauTrajetDialogComponent', () => {
  let component: NouveauTrajetDialogComponent;
  let fixture: ComponentFixture<NouveauTrajetDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NouveauTrajetDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NouveauTrajetDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
