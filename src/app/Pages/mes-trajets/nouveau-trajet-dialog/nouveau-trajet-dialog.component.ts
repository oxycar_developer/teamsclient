import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {WorkerService} from '../../../../oxy-core/CoreManager/core/CoreServices/worker.service';
import {AppEvent} from '../../../../oxy-core/events.enum';
import {location} from '../../../services/navigator.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {NouveauTrajetContentFrameComponent} from './nouveau-trajet-content-frame/nouveau-trajet-content-frame.component';

@Component({
  selector: 'app-nouveau-trajet-dialog',
  templateUrl: './nouveau-trajet-dialog.component.html',
  styleUrls: ['./nouveau-trajet-dialog.component.css']
})
export class NouveauTrajetDialogComponent implements OnInit, OnDestroy {

  $subscriber = new Subject();

  constructor(
    private dialog: MatDialog,
    private w: WorkerService
  ) {
  }

  ngOnDestroy() {
    this.$subscriber.next();
    this.dialog.closeAll();

  }

  ngOnInit(): void {


    const dialogRef = this.dialog.open(NouveauTrajetContentFrameComponent, {
      width: 'auto',
    });


    dialogRef.beforeClosed().pipe(takeUntil(this.$subscriber)).subscribe({
      next: value => {
        this.w.emit(AppEvent.RequestNavigationTo, {data: location.Mestrajets});
      }
    });
  }

}
