import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonPortefeuilleComponent } from './mon-portefeuille.component';

describe('MonPortefeuilleComponent', () => {
  let component: MonPortefeuilleComponent;
  let fixture: ComponentFixture<MonPortefeuilleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonPortefeuilleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonPortefeuilleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
