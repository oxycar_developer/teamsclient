import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MonPortefeuilleComponent} from './mon-portefeuille.component';
import {RouterModule, Routes} from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    component: MonPortefeuilleComponent,
    children: [
      {
        path: 'config',
        loadChildren: () => import('../config-dialog/config-dialog.module').then(m => m.ConfigDialogModule),
      },
    ]
  }
];

@NgModule({
  declarations: [MonPortefeuilleComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class MonPortefeuilleModule {
}
