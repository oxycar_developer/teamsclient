import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';


const ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'notfound',
    pathMatch: 'full'
  },
  {
    path: 'notfound',
    children: [
      {
        path: '',
        loadChildren: () => import('./Pages/error404/error404.module').then(m => m.Error404Module),
        data: {title: 'Error 404'}
      },
    ]
  },
  {
    path: 'trajets',
    children: [
      {
        path: '',
        loadChildren: () => import('./Pages/mes-trajets/mes-trajets.module').then(m => m.MesTrajetsModule),
        data: {title: 'Mes Trajets'}
      },
    ]
  },
  {
    path: 'demandes',
    children: [
      {
        path: '',
        loadChildren: () => import('./Pages/mes-demandes/mes-demandes.module').then(m => m.MesDemandesModule),
        data: {title: 'Mes Demandes'}
      },
    ]
  },
  {
    path: 'portefeuille',
    children: [
      {
        path: '',
        loadChildren: () => import('./Pages/mon-portefeuille/mon-portefeuille.module').then(m => m.MonPortefeuilleModule),
        data: {title: 'Mon Portefeuille'}
      },
    ]
  },
  {
    path: '**',
    redirectTo: 'notfound'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES, {preloadingStrategy: PreloadAllModules/*, enableTracing: true*/})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
