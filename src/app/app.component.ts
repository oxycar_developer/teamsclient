import {Component, OnDestroy} from '@angular/core';
import {ClientWorkerService} from '../coreClientConfig/client-worker.service';
import {serviceEnum, Services} from '../coreClientConfig/service-list';
import {ApiUtilities} from '../oxy-core/api-utilities';
import {OxyCoreService} from '../oxy-core/CoreManager/core/CoreServices/Core/oxy-core.service';
import {Day} from '../oxy-core/DataClasses/CalendarDataClass';
import {AppEvent} from '../oxy-core/events.enum';
import {ApiGpsPoint} from '../oxy-core/Swagger/swagger-result-ts/generate/model';
import {ICONS} from '../shared-web-ressources/shared-angular-web-components/Display/oxy-icons/icon/icon.component';
import {TeamsVariables} from '../shared-web-ressources/utils/teams-variables';
import {inputtype} from '../shared-web-ressources/shared-angular-web-components/Inputs/inputs/standard-input/standard-input.component';

import * as microsoftTeams from '@microsoft/teams-js';
import {Subscription} from 'rxjs';
import {C} from '../shared-web-ressources/utils/c';
import {Context} from '@microsoft/teams-js';
import UserProfile = microsoftTeams.authentication.UserProfile;
import {WorkerService} from '../oxy-core/CoreManager/core/CoreServices/worker.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {

  AppEvent = AppEvent;
  ApiUtilities = ApiUtilities;
  TeamsVariables = TeamsVariables;

  selectedTimeVar: string = '08:00';

  origin: ApiGpsPoint;
  destination: ApiGpsPoint;

  password: string;

  selectedDays: Set<Day>;

  selectedDate: Date = new Date();

  selectedValue: string = '';

  loginError: string;

  token: any;
  error: any;
  context: Context;

  title = 'TeamsClient';
  ICONS = ICONS;
  inputtype = inputtype;

  subscription: Subscription;
  window = window;

  constructor(
    private core: OxyCoreService,
    public cw: ClientWorkerService,
    public w: WorkerService,
  ) {
    // initialize the Services list and load the CoreModule
    this.core.initCore(new Services(), serviceEnum);


    //// TEAMS
    C.log('Attempt initialize microsoftTeams SDK...');
    microsoftTeams.initialize(() => {
      C.log('initialize microsoftTeams SDK :::: SUCCESS');

      microsoftTeams.getContext((context) => {
        console.log('microsoftTeams ::: getCONTEXT', context);
        this.context = context;
        this.cw.data().auth.voidLoginTeams.email = context.userPrincipalName;
        this.cw.data().auth.voidLoginTeams.tid = context.tid;

      });


      microsoftTeams.authentication.getAuthToken({
        successCallback: (token1) => {
          console.log('microsoftTeams ::: Success: ' + token1);
          this.token = token1;
          this.error = null;
          this.cw.data().auth.voidLoginTeams.token = token1;
          this.cw.emit(AppEvent.RequestTeamsLogin);
        },
        failureCallback: (error) => {
          this.error = error;
          this.token = null;
          console.log('microsoftTeams ::: Failure: ' + error);
        },
      });
    });


    //// TEAMS

    this.cw.listenForEvent(AppEvent.LoginError).subscribe({
      next: value => {
        this.loginError = value;
      }
    });

  }

  ngOnDestroy() {
  }

  public log(x: any) {
    console.log(x);
  }


}


