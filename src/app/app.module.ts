import {BrowserModule} from '@angular/platform-browser';
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';

import {AppComponent} from './app.component';
import {OxyCoreModule} from '../coreClientConfig/oxy-core.module';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AgmCoreModule} from '@agm/core';
import {ButtonModule} from '../shared-web-ressources/shared-angular-web-components/Inputs/button/button.module';
import {OxyIconsModule} from '../shared-web-ressources/shared-angular-web-components/Display/oxy-icons/oxy-icons.module';
import {AppRoutingModule} from './app-routing.module';
import {CardModule} from '../shared-web-ressources/shared-angular-web-components/Display/card/card.module';
import {InputsModule} from '../shared-web-ressources/shared-angular-web-components/Inputs/inputs/inputs.module';
import {FormLoginModule} from '../shared-web-ressources/shared-angular-web-components/Forms/form-login/form-login.module';


export function loggerCallback(logLevel, message, piiEnabled) {
  console.log(message);
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    OxyCoreModule,
    FormsModule,
    CardModule,
    BrowserAnimationsModule,
    OxyIconsModule,
    ButtonModule,
    InputsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCYd3GMn2DEJF9xwav3cVIKra5RMEuS_Ps',
      libraries: ['places']
    }),
    AppRoutingModule,
    FormLoginModule,

  ],
  providers: [],
  bootstrap:
    [AppComponent]
  , schemas: [NO_ERRORS_SCHEMA]
})

export class AppModule {
}
