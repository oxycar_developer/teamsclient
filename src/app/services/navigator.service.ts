import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {AbstractService} from '../../oxy-core/CoreManager/core/CoreServices/Core/abstract.service';
import {WorkerService} from '../../oxy-core/CoreManager/core/CoreServices/worker.service';
import {AppEvent} from '../../oxy-core/events.enum';
import {ApiUtilities} from '../../oxy-core/api-utilities';
import {C} from '../../shared-web-ressources/utils/c';
import {Browser} from 'leaflet';
import win = Browser.win;


@Injectable({
  providedIn: 'root'
})

export class NavigatorService extends AbstractService {


  constructor(
    protected w: WorkerService,
    private router: Router,
  ) {
    super(w);

    this.linkEvents([
      {event: AppEvent.RequestNavigationTo, callback: (value) => this.goTo(value.data)},
    ]);
  }

  setTarget(target: string) {
    ApiUtilities.log('storing target : ', target);
    if (target.split('?previousPage=')[1]) {
      ApiUtilities.setStorageItem('targetPreviousPage', target.split('?previousPage=')[1]);
    }
    ApiUtilities.setStorageItem('target', target.split('?previousPage=')[0]);
  }

  goToTarget(alternative: location) {
    const target: string = ApiUtilities.getStorageItem('target', true);
    const targetPreviousPage: string = ApiUtilities.getStorageItem('targetPreviousPage', true);
    if (target) {
      ApiUtilities.log('goint to target : ', target);
      if (targetPreviousPage) {
        this.router.navigate([target], {queryParams: {previousPage: targetPreviousPage}});
      } else {
        this.router.navigate([target]);
      }
    } else {
      ApiUtilities.log('no target found : ', target, 'going to alternative : ', alternative);
      this.goTo(alternative);
    }
  }


  goTo(l: location, params = null) {

    let path = '';
    switch (l) {
      case location.Home:
        if (window.location.pathname.includes('trajets')) {
          path = '/trajets';
        } else if (window.location.pathname.includes('demandes')) {
          path = '/demandes';
        } else if (window.location.pathname.includes('portefeuille')) {
          path = '/portefeuille';
        } else {
          path = 'error';
        }
        break;
      case location.Auth:
        path = '/authentication';
        break;
      case location.ConfigAddresses:
        C.log('window.location.pathname ::::', window.location.pathname);
        if (window.location.pathname.includes('trajets')) {
          path = '/trajets/config';
        } else if (window.location.pathname.includes('demandes')) {
          path = '/demandes/config';
        } else if (window.location.pathname.includes('portefeuille')) {
          path = '/portefeuille/config';
        } else {
          path = 'error';
        }
        break;
      case location.ConfigSNCF:
        path = window.location.pathname + '/config';
        break;
      case location.Mestrajets:
        path = '/trajets';
        break;
      case location.Mesdemandes:
        path = '/demandes';
        break;
      case location.Monportefeuille:
        path = '/portefeuille';
        break;
      default:
        break;
    }
    ApiUtilities.log('going to target : ', path);
    this.router.navigate([path]);


  }

}


export enum location {
  Home,
  Auth,
  Mestrajets,
  Mesdemandes,
  Monportefeuille,
  ConfigAddresses,
  ConfigSNCF,
}
