import {Configuration as AuthConfig} from '../oxy-core/Swagger/swagger-result-ts/generate';
import {Configuration as BackendConfig} from '../oxy-core/Swagger/swagger-result-ts/generate';
import {environment} from '../environments/environment';


export function AuthConfiguration(): AuthConfig {
  ApiConfigurationUtils.log('location:', window.location.hostname);
  return new AuthConfig({
      apiKeys: {
        version: ApiConfigurationUtils.version().version + ' - ' + ApiConfigurationUtils.levelstring()
      },
      basePath: ApiConfigurationUtils.getAuthPath(), /// on Prod only https works
    }
  );
}

export function BackendConfiguration(): BackendConfig {
  return new BackendConfig(
    {
      apiKeys: {
        version: ApiConfigurationUtils.version().version + ' - ' + ApiConfigurationUtils.levelstring()
      },
      basePath: '', /// on Prod only https works
    }
  );
}

export function ApiConfigurationClass(): ApiConfiguration {
  return new ApiConfiguration();
}

declare const require: any;

export const version = {
  production: false,
  version: require('../../package.json').version,
  name: require('../../package.json').name,
  license: require('../../package.json').license,
};


export class ApiConfigurationUtils {

  static getAuthPath(): string {
    let path: string;
    if (localStorage.getItem('Customauthpath')) {
      path = localStorage.getItem('Customauthpath');
      console.log('AuthPath is CUSTOM --> ', path);
    } else if (this.level() <= Level.alpha) {
      path = 'https://api-alpha-auth.oxy-car.com';
      console.log('AuthPath is ALPHA --> ', path);
    } else if (this.level() === Level.betatest) {
      path = 'https://api-beta-auth.oxy-car.com';
      console.log('AuthPath is BETA-TEST --> ', path);
    } else { // includes beta.oxy-car.com
      path = 'https://api-prod-auth.oxy-car.com';
      console.log('AuthPath is PRODUCTION --> ', path);
    }
    return path;
  }

  static version(): { license: any; name: any; version: any } {
    return version;
  }


  /**
   * @return 0: debug, 1: alpha, 2: beta-test, 3: beta, 4: prod
   */
  static level(): Level {
    const host = window.location.hostname;
    let level: Level;
    try {
      const customLevel: Level = this.getStorageItem('customLevel');
      if (customLevel) {
        return customLevel;
      }
    } finally {
    }

    if (environment.production) {
      if (
        host === 'beta.oxy-car.com'
      ) {
        level = Level.beta;
      } else if (
        host === 'beta-test.oxy-car.com'
      ) {
        level = Level.betatest;
      } else if (
        host === 'alpha.oxy-car.com'
      ) {
        level = Level.alpha;
      } else {
        level = Level.production;
      }
    } else {
      level = Level.debug;
    }
    return level;
  }


  static levelstring(): string {
    switch (this.level()) {
      case Level.debug:
        return 'debug';
      case Level.alpha:
        return 'alpha';
      case Level.betatest:
        return 'beta-test';
      case Level.beta:
        return 'beta';
      case Level.production:
        return 'production';
      default:
        return 'debug';
    }
  }


  static getStorageItem(key: string, deleteafter: boolean = false): any {
    let item = null;
    try {
      item = JSON.parse(localStorage.getItem(key));
      if (deleteafter) {
        localStorage.removeItem(key);
      }
    } catch (err) {
      this.log(err, key, item);
      item = null;
    }
    return item;
  }


  static log(...x: any) {
    if ((this.level() <= Level.alpha || this.getStorageItem('LoggerActif')) && !this.getStorageItem('LoggerDisabled')) {
      console.log(x);
    }
  }


}

export enum Level {
  debug,
  alpha,
  betatest,
  beta,
  production
}


export class ApiConfiguration {

  constructor() {
  }

  getAuthPath(): string {
    return ApiConfigurationUtils.getAuthPath();
  }

}
