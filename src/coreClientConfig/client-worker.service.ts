import {Injectable} from '@angular/core';
import {ClientDataObject} from './model/client-data-object';
import {CustomWorkerServiceTemplateService} from '../oxy-core/CoreManager/core/CoreServices/custom-worker-service-template.service';
import {WorkerService} from '../oxy-core/CoreManager/core/CoreServices/worker.service';



@Injectable({
  providedIn: 'root'
})
export class ClientWorkerService extends CustomWorkerServiceTemplateService {

  public clientData: ClientDataObject = new ClientDataObject();

  constructor(protected w: WorkerService) {
    super(w);
  }

  clientdata() {
    return this.clientData.data;
  }

}
