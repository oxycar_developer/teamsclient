import {Observable}                from 'rxjs';
import {ClientDataClass} from '../client-data-class';
import {OxyCustomBehaviourSubject} from '../../oxy-core/CoreManager/core/CoreDataClasses/Core/oxy-core-behaviour-subject';


export class ClientDataObject {

  /// Data Variables go here ->
  // tslint:disable-next-line:variable-name
  private _data: OxyCustomBehaviourSubject<ClientDataClass>
            = new OxyCustomBehaviourSubject<ClientDataClass>(new ClientDataClass());

  get data() {
    return this._data.value;
  }

  set data(val: any) {
    this._data.value = val;
  }

  constructor() {
  }

  observe(): Observable<any> {
    return this._data.observe;
  }


}
