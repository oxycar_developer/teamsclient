import {NgModule} from '@angular/core';


import {ApiConfigurationClass, AuthConfiguration, BackendConfiguration} from './api.configuration';
import {
  ApiModule as BackendModule,
} from '../oxy-core/Swagger/swagger-result-ts/generate';
import {ApiModule as AuthModule} from '../oxy-core/Swagger/swagger-result-ts/generate';
import {SwaggerModuleModule} from '../oxy-core/Swagger/swagger-module.module';
import {CoreModule} from '../oxy-core/CoreManager/core/core.module';


@NgModule({
  imports: [
    CoreModule,
    AuthModule.forConfig(AuthConfiguration),
    BackendModule.forConfig(BackendConfiguration),
    SwaggerModuleModule.forConfig(ApiConfigurationClass)
  ],
})
export class OxyCoreModule {
}
