import {AppEvent} from '../oxy-core/events.enum';
import {OxyDesignService} from '../oxy-core/Services/oxy-design.service';
import {UserService} from '../oxy-core/Services/user.service';
import {AuthService} from '../oxy-core/Services/auth.service';
import {NavigatorService} from '../app/services/navigator.service';
import {EntrepriseService} from '../oxy-core/Services/entreprise.service';


export class Services {
  list = [
    {name: 'auth', service: AuthService, initOn: AppEvent.init},
    {name: 'design', service: OxyDesignService, initOn: AppEvent.init},
    {name: 'user', service: UserService, initOn: AppEvent.LoginSuccess},
    {name: 'navigator', service: NavigatorService, initOn: AppEvent.init},
    {name: 'entreprise', service: EntrepriseService, initOn: AppEvent.LoginSuccess},
  ];
}

export enum serviceEnum {
  auth,
  design,
  user,
  navigator,
  entreprise
}




