import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from './app/app.module';
import {environment} from './environments/environment';
import {C} from './shared-web-ressources/utils/c';

if (environment.production) {
  enableProdMode();
}


if (C.environment().production) {
  console.log(C.version().name, ' version:', C.version().version, '- production');
  enableProdMode();
} else {
  console.log(C.version().name, ' version:', C.version().version, '- development');
}
console.log('License: ', C.version().license);
console.log('Location: ', window.location.href);


platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
